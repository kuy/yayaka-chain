type t = {
  hash_algorithm : HashAlgorithm.t;
  signature_algorithm : string;
  fingerprint : string;
  body : string }
